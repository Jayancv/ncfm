@extends('layouts.master')


@section('content')
        <form class="form-horizontal"  method="post"
              enctype="multipart/form-data" role="form">

        <div>
            <label for="username">Name</label>
            <input class="mdl-textfield__input" type="text" id="name" name="name">

        </div>

        </br>
        <div class="mdl-textfield mdl-js-textfield ">
            <label>Goal</label>
            <select type="text" name="designation" class="mdl-textfield__input" id="designation">
                @foreach($designations as $designation)
                    <option value={{$designation->getDesignationId()}} >{{$designation->getDesignationDescription()}}</option>
                @endforeach
            </select>
        </div>
        <br>
        <div class="mdl-textfield mdl-js-textfield ">
            <label>Interested field</label>
            <select type="text" name="stream" class="mdl-textfield__input" id="designation">
                @foreach($groups as $group)
                    <option value={{$group->getGroupDescription()}} >{{$group->getGroupDescription()}}</option>
                @endforeach
            </select>
        </div>

        <div class="mdl-textfield mdl-js-textfield ">
            <label>Grade Completed</label>
            <select name="grade" class="mdl-textfield__input" id="grade">
                <option value="8">Grade 8</option>
                <option value="9">Grade 9</option>
                <option value="10">Grade 10</option>
                <option value="11">Grade 11</option>
                <option value="13">Grade 13</option>
            </select>
            {{--<script type="text/javascript">--}}
            {{--var c = document.getElementById('stream');--}}
            {{--var d = document.getElementById('grade');--}}

            {{--d.addEventListener('change', function () {--}}
            {{--console.log(d.value);--}}


            {{--if (d.value == '13') {--}}

            {{--document.getElementById('stream').disabled = false;--}}
            {{--}--}}
            {{--else {--}}
            {{--document.getElementById('stream').disabled = true;--}}
            {{--}--}}
            {{--})--}}


            {{--</script>--}}


        </div>
        </br>

        <div class="mdl-textfield mdl-js-textfield ">
            <label>A/L Stream</label>
            <select type="text" name="stream" class="mdl-textfield__input" id="stream">
                <option value="Mathematics">Mathematics</option>
                <option value="Biology">Biology</option>
                <option value="Commerce">Commerce</option>
                <option value="Art">Art</option>
                <option value="Other">Other</option>
            </select>


        </div>

        </br>


        <button type="submit" style="left:300px" ;>
            Submit
        </button>


    </form>

@endsection

