@extends('layouts.master')

@section('content')

    <div class="container-fluid row">
    <center>
    <div class="panel">
        <h3>{{$details->institution->getInstitutionDescription()}}</h3>
        <h4>{{$details->course->getCourseDescription()}}</h4>
        <h5>Course fee (Full) :Rs {{$details->getCost()}}</h5>
        <h5>Course duration : {{$details->getDuration()}} Months</h5>
    <div>
    </center>
    </div>
@endsection