
<html>
<head>
    <title>Career Path</title>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <!-- Material Design Lite -->
    <script src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <!-- Material Design icon font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Wide card with share menu button -->

    </div>
    <style>
        .demo-layout-transparent {
            background: url('http://www.wolfexperience.com/wp-content/uploads/2016/01/social-media.jpg') center / cover;
        }
        .demo-layout-transparent .mdl-layout__header,
        .demo-layout-transparent .mdl-layout__drawer-button {
            /* This background is dark, so we set text to white. Use 87% black instead if
               your background is light. */
            color: white;
        }
        .demo-card-wide.mdl-card {
            width: 950px;

            height: 500px;
            position: absolute;
            left: 200px;
            top:150px;
        }
        .demo-card-wide > .mdl-card__title {
            color: #fff;
            height: 176px;
            background: url('http://www2.psd100.com/wp-content/uploads/2013/08/Blue-technology-background20130812.jpg') center / cover;
        }
        .demo-card-wide > .mdl-card__menu {
            color: #fff;
        }

        body {

            background: #fafafa;
            position: relative;
        }


    </style>
</head>
<body>
<!-- Uses a transparent header that draws on top of the layout's background -->
<div class="demo-layout-transparent mdl-layout mdl-js-layout ">
    <header class="mdl-layout__header mdl-layout__header--transparent">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Future Minds - Learning Path</span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="/learningPath">Learning Path</a>
                <a class="mdl-navigation__link" href="/demoView">Demo</a>


            </nav>
        </div>
    </header>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Welcome</h2>

        </div>
        <div class="mdl-card__supporting-text">
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                Get Started
            </a>

        </div>
        <div class="mdl-card__menu">
            <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                <i class="material-icons">share</i>
            </button>
        </div>
    </div>
    <div class="mdl-layout__drawer" >
        <span class="mdl-layout-title">Menu</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="/learningPath">Learning Path</a>

        </nav>
    </div>
    <main class="mdl-layout__content">
    </main>
</div>
</body>
</html>