<DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Career Path</title>
        <script src="js/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/vis.css" rel="stylesheet"/>
        <link href="css/sidebar.css" rel="stylesheet"/>
        <link href="css/slick.css" rel="stylesheet"/>

        <script src="js/vis.js"></script>
        <script src="js/d3.v3.min.js"></script>

        <!-- drawer.css -->
        <link rel="stylesheet" href="css/drawer.min.css"/>
        <!-- jquery & iScroll -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <script src="js/iscroll.js"></script>
        <!-- drawer.js -->
        <script src="js/drawer.min.js"></script>

    </head>
    <style>

        .node {
            cursor: pointer;
        }

        .node circle {
            stroke-width: 3px;
        }

        .node rect {
            stroke-width: 3px;
        }

        .node text {
            font: 12px sans-serif;
            fill: #000;
        }

        .link {
            fill: none;
            stroke: #ccc;
            stroke-width: 10px;
        }

        .link:hover {
            cursor: pointer;
            stroke: yellow;
        }

        .tree {
            margin-bottom: 10px;
            overflow: auto;
        }

    </style>


    <body class="drawer drawer--left">

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">View Paths</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#" id="btnCost">By Cost</a></li>
                    <li><a href="#" id="btnDuration">By Duration</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
        </nav>


    <header role="banner">

        <nav class="drawer-nav" role="navigation" style="padding-top:50px">
            <ul class="drawer-menu" id="drawer-items">
                <li><a class="drawer-brand" href="#">Course</a></li>
                <li><a class="drawer-menu-item" href="#">Nav1</a></li>
                <li><a class="drawer-menu-item" href="#">Nav2</a></li>
            </ul>
        </nav>
    </header>
    <script>
        $(document).ready(function () {
            $('.drawer').drawer();

            $("#btnCost").click(function(){
                $("#cont_tree_2").hide(200);
                $("#cont_tree_1").show(200);
            });

            $("#btnDuration").click(function(){
                $("#cont_tree_1").hide(200);
                $("#cont_tree_2").show(200);

            });
        });

        $('.drawer').drawer({
            class: {
                nav: 'drawer-nav',
                toggle: 'drawer-toggle',
                overlay: 'drawer-overlay',
                open: 'drawer-open',
                close: 'drawer-close',
                dropdown: 'drawer-dropdown'
            },
            iscroll: {
                // Configuring the iScroll
                // https://github.com/cubiq/iscroll#configuring-the-iscroll
                mouseWheel: true,
                preventDefault: false
            },
            showOverlay: true
        });

        $('.drawer').on('drawer.opened', function () {

            /*
             your logic here

             */

        });

    </script>



    <main role="main">
        <div id="cont_tree_1">
            <div id="tree" style="width100%">

            </div>
        </div>

        <div style="display:none" id="cont_tree_2">
            <div id="tree2" style="width100%;">

            </div>
        </div>
    </main>
    <script>

        window.paths = JSON.parse('{!! json_encode($paths)  !!}');



        var node_dic = window.paths[0]["node_dic"];
        var node_set = window.paths[0]["node_set"];
        var root_node = window.paths[0]["start_node"];
        var end_node = window.paths[0]["end_node"];
        var path_info = window.paths[0]["path_info"];
        var sp_info = window.paths[0]["shortest_path"];

        var root = createTree(root_node);
        tree = BuildVerticaLTree(root, '#tree', node_set, path_info);
        var endNode = findFromRoot2(root, end_node, sp_info['sp_path']);
        tree.nodeclick(endNode);


        var node_dic = window.paths[1]["node_dic"];
        var node_set = window.paths[1]["node_set"];
        var root_node = window.paths[1]["start_node"];
        var end_node = window.paths[1]["end_node"];
        var path_info = window.paths[1]["path_info"];
        var sp_info = window.paths[1]["shortest_path"];

        var root2 = createTree(root_node);
        tree2 = BuildVerticaLTree(root2, '#tree2', node_set, path_info);
        var endNode2 = findFromRoot(root2, end_node,sp_info['sp_path']);
        tree2.nodeclick(endNode2);

        function findFromRoot(root, d){
            while (root.name !== d) {
                root = root.children[0];
            }
            return root;
        }
        function findFromRoot2(root, d, path) {
          console.log(path);
          for(var i=1;i<path.length;i++){
            for(var j=0;j<root.children.length;j++){
              if(root.children[j].name === path[i]){
                root = root.children[j];
                break;
              }
            }
          }

          return root;
        }


        function createTree(node) {
            var struct = {}
            struct['name'] = node;
            if (node_dic.hasOwnProperty(node)) {
                var child = [];
                for (var adj in node_dic[node]) {
                    child.push(createTree(node_dic[node][adj]));
                }
                struct['children'] = child;
            }
            return struct;

        }

        function BuildVerticaLTree(treeData, treeContainerDom, inode_set, ipath_info) {
            var margin = {top: 40, right: 120, bottom: 20, left: 120};
            var width = window.screen.width - margin.right - margin.left;
            var height = window.screen.height - margin.top - margin.bottom;

            var i = 0, duration = 750;
            var tree = d3.layout.tree()
                .size([height, width]);
            var diagonal = d3.svg.diagonal()
                .projection(function (d) {
                    return [d.x, d.y];
                });
            var svg = d3.select(treeContainerDom).append("svg")
                .attr("width", width + margin.right + margin.left)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            root = treeData;

            update(root);

            var circle_radius = 40;
            var rect_height = 30;
            var rect_width = 100;


            function update(source) {
                // Compute the new tree layout.
                var nodes = tree.nodes(root).reverse(),
                    links = tree.links(nodes);
                // Normalize for fixed-depth.
                nodes.forEach(function (d) {
                    d.y = d.depth * 100;
                });
                // Declare the nodes&hellip;
                var node = svg.selectAll("g.node")
                    .data(nodes, function (d) {
                        return d.id || (d.id = ++i);
                    });
                // Enter the nodes.
                var nodeEnter = node.enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + source.x0 + "," + source.y0 + ")";
                    }).on("click", nodeclick);
                nodeEnter.append("rect")
                    .attr("stroke", function (d) {
                        return d.children || d._children ?
                            "steelblue" : "#00c13f";
                    })
                    .style("fill", function (d) {
                        return d.children || d._children ?
                            "lightsteelblue" : "#fff";
                    });
                //.attr("r", 10)
                //.style("fill", "#fff");
                nodeEnter.append("text")
                // .attr("y", function (d) {
                //     return d.children || d._children ? -18 : 18;
                // })
                    .attr("y", function (d) {
                        return 0;
                    })
                    .attr("dy", ".35em")
                    .attr("text-anchor", "middle")
                    .style("word-wrap", "break-word")
                    .text(function (d) {
                        return inode_set[d.name];
                    }) //change node text here
                    .style("fill-opacity", 1e-6);
                // Transition nodes to their new position.
                //horizontal tree
                var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + d.x +
                            "," + d.y + ")";
                    });
                nodeUpdate.select("rect")
                    .attr("height", rect_height)
                    .attr("width", rect_width)
                    .attr("x", -rect_width / 2)
                    .attr("y", -rect_height / 2)
                    .style("border-radius", 5)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });
                nodeUpdate.select("text")
                    .style("fill-opacity", 1);

                // Transition exiting nodes to the parent's new position.
                var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + source.x +
                            "," + source.y + ")";
                    })
                    .remove();
                nodeExit.select("circle")
                    .attr("height", 1e-6)
                    .attr("width", 1e-6);
                nodeExit.select("text")
                    .style("fill-opacity", 1e-6);
                // Update the links&hellip;
                // Declare the links&hellip;
                var link = svg.selectAll("path.link")
                    .data(links, function (d) {
                        return d.target.id;
                    });
                // Enter the links.
                link.enter().insert("path", "g")
                    .attr("class", "link")

                    .attr("d", function (d) {
                        var o = {x: source.x0, y: source.y0};
                        return diagonal({source: o, target: o});
                    });
                // Transition links to their new position.
                link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

                link.on('click', edgeClick);

                // Transition exiting nodes to the parent's new position.
                link.exit().transition()
                    .duration(duration)
                    .attr("d", function (d) {
                        var o = {x: source.x, y: source.y};
                        return diagonal({source: o, target: o});
                    })
                    .remove();

                // Stash the old positions for transition.
                nodes.forEach(function (d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                });
            }

            // Toggle children on click.
            function nodeclick(d) {
                //find the parent hierrarchy
                var parents = findParentNodes(d);
                collapseAll(parents[parents.length - 1]);

                for (var i in parents) {
                    var node = parents[i];
                    if (!node.children) {
                        node.children = node._children;
                        node._children = null;
                    }
                    update(node);
                }
            }

            function edgeClick(d) {
                $('.drawer').drawer('open');
                var path = ipath_info[d.source.name][d.target.name];
                $("#drawer-items").empty();
                $("#drawer-items").append('<li><a class="drawer-brand">' + path.course + '</a></li>');

                  path.institutions.forEach(function(i){
                    $("#drawer-items").append('<li><a class="drawer-menu-item" href="/details/'+i['id']+'" target="_blank">'+ i['name'] +'</a></li>');
                  });
            }

            function collapseAll(d) {
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapseAll);
                    d.children = null;
                }
                update(d);
            }

            function findParentNodes(current) {
                var nodes = Array();
                nodes.push(current);
                while (current.parent) {
                    nodes.push(current.parent);
                    current = current.parent;
                }
                return nodes;
            }

            this.nodeclick = nodeclick;
            this.update = update;
            return this;
        }


    </script>

    </body>
    </html>
