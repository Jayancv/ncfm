<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Career Path</title>
    <script src="js/jquery.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    
    <script src="js/bootstrap.min.js"></script>
    <link href="css/training.css" rel="stylesheet"/>
    <script src="js/d3.js"></script>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    <!-- Material Design icon font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <style>
        .demo-layout-transparent {
            /*background: url('/Images/social-media.jpg') center / cover;*/
        }

        .demo-layout-transparent .mdl-layout__header,
        .demo-layout-transparent .mdl-layout__drawer-button {
            /* This background is dark, so we set text to white. Use 87% black instead if
               your background is light. */
            color: white;
        }

        .demo-card-wide.mdl-card {
            width: 950px;

            height: 650px;
            position: absolute;
            left: 200px;
            top: 50px;
        }

        .demo-card-wide > .mdl-card__title {
            color: #fff;
            height: 176px;
            /*background: url('http://www2.psd100.com/wp-content/uploads/2013/08/Blue-technology-background20130812.jpg') center / cover;*/
        }

        .demo-card-wide > .mdl-card__menu {
            color: #fff;
        }

        .demo-list-control {
            width: 300px;
        }

        .demo-list-radio {
            display: inline;
        }

        body {
            padding: 20px;
            background:  #9923B3;
            position: relative;
        }


    </style>


</head>
<body class="demo-layout-transparent">

<div id="content">
    <div class="container-fluid col-md-12">
        <div>
            <div>
                @yield('content')
            </div>
        </div>
    </div>

</div>
<div class="footer col-md-12">Future Minds &copy; Copyright Notice</div>

<!-- Vendor Scripts Bundle -->
<script src="{{asset('js/vendor.min.js')}}"></script>

<!-- App Scripts Bundle -->
<script src="{{asset('js/scripts.min.js')}}"></script>


</body>
</html>
