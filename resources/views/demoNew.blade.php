{{--<DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--<meta charset="utf-8" />--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1" />--}}
{{--<title>Career Path</title>--}}
{{--<script src="js/jquery.min.js"></script>--}}
{{--<link href="css/bootstrap.min.css" rel="stylesheet"/>--}}
{{--<script src="js/bootstrap.min.js"></script>--}}
{{--<link href="css/training.css" rel="stylesheet"/>--}}
{{--<script src="js/d3.js"></script>--}}
{{--</head>--}}
{{--<body>--}}
{{--<form class="form-horizontal" role="form"  action="{{ route('process') }}" method="post">--}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Curriculum :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="cur">--}}
{{--<option value="">Select</option>--}}
{{----}}
{{--@if(isset($curs))--}}
{{--@foreach($curs as $c)--}}
{{----}}
{{--<option value="{{$c->curriculum_Id}}">{{$c->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Qualification :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="qual">--}}
{{--<option value="">Select</option>--}}
{{--@if(isset($quals))--}}
{{--@foreach($quals as $q)--}}
{{--<option value="{{$q->qualification_Id}}">{{$q->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Designation :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="desig">--}}
{{--<option value="">Select</option>--}}
{{--@if(isset($desigs))--}}
{{--@foreach($desigs as $d)--}}
{{--<option value="{{$d->designation_Id}}">{{$d->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{----}}
{{--<div class="col-sm-4">--}}
{{--<input type="submit" class="btn btn-primary" value="Submit" />--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{----}}
{{--</form>--}}
{{--</body>--}}--}}
{{--</html>--}}

{{--</html>--}}

@extends('layouts.master')

@section('content')

    <div class="container-fluid row">

        <div class="col-md-12">
            <div class="panel panel-default ">
                <div class="panel-heading">Testing Form</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" action="{{ route('process') }}" method="post">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Curriculum :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="cur">
                                    <option value="">Select</option>

                                    @if(isset($curs))
                                        @foreach($curs as $c)

                                            <option value="{{$c->curriculum_Id}}">{{$c->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Qualification :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="qual">
                                    <option value="">Select</option>
                                    @if(isset($quals))
                                        @foreach($quals as $q)
                                            <option value="{{$q->qualification_Id}}">{{$q->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Designation :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="desig">
                                    <option value="">Select</option>
                                    @if(isset($desigs))
                                        @foreach($desigs as $d)
                                            <option value="{{$d->designation_Id}}">{{$d->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-4">
                                <input type="submit" class="btn btn-primary" value="Submit"/>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var c = document.getElementById('stream');
        var d = document.getElementById('grade');

        d.addEventListener('change', function () {

            if (d.value == '13') {

                document.getElementById('stream').disabled = false;
            }
            else {
                document.getElementById('stream').disabled = true;
            }
        })


    </script>
@endsection
