{{--<DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--<meta charset="utf-8" />--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1" />--}}
{{--<title>Career Path</title>--}}
{{--<script src="js/jquery.min.js"></script>--}}
{{--<link href="css/bootstrap.min.css" rel="stylesheet"/>--}}
{{--<script src="js/bootstrap.min.js"></script>--}}
{{--<link href="css/training.css" rel="stylesheet"/>--}}
{{--<script src="js/d3.js"></script>--}}
{{--</head>--}}
{{--<body>--}}
{{--<form class="form-horizontal" role="form"  action="{{ route('process') }}" method="post">--}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Curriculum :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="cur">--}}
{{--<option value="">Select</option>--}}
{{----}}
{{--@if(isset($curs))--}}
{{--@foreach($curs as $c)--}}
{{----}}
{{--<option value="{{$c->curriculum_Id}}">{{$c->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Qualification :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="qual">--}}
{{--<option value="">Select</option>--}}
{{--@if(isset($quals))--}}
{{--@foreach($quals as $q)--}}
{{--<option value="{{$q->qualification_Id}}">{{$q->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{--<div class="col-sm-2">--}}
{{--<label class="control-label" for="line1">Select Designation :</label>--}}
{{--</div>--}}
{{--<div class="col-sm-4">--}}
{{--<select class="form-control" name="desig">--}}
{{--<option value="">Select</option>--}}
{{--@if(isset($desigs))--}}
{{--@foreach($desigs as $d)--}}
{{--<option value="{{$d->designation_Id}}">{{$d->description}}</option>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</select>--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{--<div class="form-group">--}}
{{----}}
{{--<div class="col-sm-4">--}}
{{--<input type="submit" class="btn btn-primary" value="Submit" />--}}
{{--</div>            --}}
{{--</div>--}}
{{----}}
{{----}}
{{--</form>--}}
{{--</body>--}}
{{--</html>--}}
<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Roboto:400);
body {
  background-color:#fff;
  -webkit-font-smoothing: antialiased;
  font: normal 14px Roboto,arial,sans-serif;
}

.container {
    padding: 25px;
    position: fixed;
}

.form-login {
    background-color: #EDEDED;
    padding-top: 10px;
    padding-bottom: 20px;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 15px;
    border-color:#d2d2d2;
    border-width: 5px;
    box-shadow:0 1px 0 #cfcfcf;
    width: 80%;
    text-align: left;

}

h4 { 
 border:0 solid #fff; 
 border-bottom-width:1px;
 padding-bottom:10px;
 text-align: left;
}

.form-control {
    border-radius: 10px;
}

.wrapper {
    text-align: center;
}

.mycontainer {
    padding: 10px;
}
</style>
@extends('layouts.master')

@section('content')

<script type="text/javascript">

    $( document ).ready(function() {
    
        var al_subjects = "";
        $("#al_subjects").hide();
        $("#qualification").change(function() {


            if($(this).val() == 'Q3'){
                 $("#al_subjects").show(); 
                 al_subjects = JSON.parse('{!! json_encode($al_subjects)  !!}');  
                 console.log(al_subjects);
            }else{
                $("#al_subjects").hide(); 
            }

           if($(this).val() == 'Q1' || $(this).val() == 'Q2'){
            $("#curriculum").val('C0');
            $('#curriculum option[value!="C0"]').hide();
           }else{
            $("#curriculum").val('');
            $('#curriculum option[value!="C0"]').show();
           }
        });

        $("#curriculum").change(function() {

            $('#subject1').html('');
            $('#subject2').html('');
            $('#subject3').html('');
            $('#subject1').append('<option value="" selected>SELECT YOUR SUBJECT</option>');
            $('#subject2').append('<option value="" selected>SELECT YOUR SUBJECT</option>');
            $('#subject3').append('<option value="" selected>SELECT YOUR SUBJECT</option>');
            

            var stream = "";
            if($(this).val() == 'C10'){
                 stream = "Physical Science";
            }else if($(this).val() == 'C11'){
                ;
            }else if($(this).val() == 'C12'){
                ;
            }else if($(this).val() == 'C13'){
                ;
            }
            
            $.each(al_subjects, function(key,value){
                if(value.subject_stream == stream){
                    $('#subject1').append($('<option>', {value:value.al_subject_id, text:value.subject_name}));
                    $('#subject2').append($('<option>', {value:value.al_subject_id, text:value.subject_name}));
                    $('#subject3').append($('<option>', {value:value.al_subject_id, text:value.subject_name}));
                }
            });
        });

    });
   
</script>
    <div>

        <div >
            <div class="panel panel-default ">
                <div class="panel-heading"><center><label style="font-weight:bold;font-size:20px;">Future Minds - Career Path</label></center></div>
                <div class="panel-body">
                    <center>
                    <form class="form-horizontal" role="form" action="{{ route('process') }}" method="post">
                    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

                    <div class="mycontainer">
                        <div class="row">
                            <div>
                            <div class="form-login">
                            <h4>SELECT CURRENT QUALIFICATION</h4>
                        

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label" for="line1">Select Qualification :</label>
                                </div>

                                <div class="col-md-6">
                                    <select class="form-control" name="qual" id="qualification" required="true">
                                        <option value="">Select</option>
                                        @if(isset($quals))
                                            @foreach($quals as $q)
                                                <option value="{{$q->qualification_Id}}">{{$q->description}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="conrol-label" for="line1">Select Curriculum :</label>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control" name="cur" id="curriculum" required="true">
                                        <option value="">Select</option>

                                        @if(isset($curs))
                                            @foreach($curs as $c)

                                                <option value="{{$c->curriculum_Id}}">{{$c->description}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                <div class="" id="al_subjects">
                <div class="form-group subjectgrouppanel">

                        <label for="inputEmail3" class="col-sm-2 control-label">First Subject</label>

                        <div class="col-sm-10 col-md-4">
                            <select class="form-control" name="subject1" id="subject1">
                                <option value="" selected>SELECT YOUR SUBJECT</option>
                            </select>
                        </div>

                        <div class="col-sm-10 col-md-4 ">
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject1" id="inlineRadio1" value="option1"> A
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject1" id="inlineRadio2" value="option2"> B
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject1" id="inlineRadio3" value="option3"> C
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject1" id="inlineRadio3" value="option3" checked> S
                            </label>
                        </div>
                    </div>
                    <div class="form-group subjectgrouppanel">
                        <label for="inputEmail3" class="col-sm-2 control-label">Second Subject</label>

                        <div class="col-sm-10 col-md-4">
                            <select class="form-control" name="subject2" id="subject2">
                                <option value="" selected>SELECT YOUR SUBJECT</option>
                            </select>
                        </div>
                        <div class="col-sm-10 col-md-4 ">
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject2" id="inlineRadio1" value="option1"> A
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject2" id="inlineRadio2" value="option2"> B
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject2" id="inlineRadio3" value="option3"> C
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject2" id="inlineRadio3" value="option3" checked> S
                            </label>
                        </div>
                    </div>
                    <div class="form-group subjectgrouppanel">
                        <label for="inputEmail3" class="col-sm-2 control-label">Third Subject</label>

                        <div class="col-sm-10 col-md-4">
                            <select class="form-control" name="subject3" id="subject3">
                                <option value="" selected>SELECT YOUR SUBJECT</option>
                            </select>
                        </div>
                        <div class="col-sm-10 col-md-4 ">
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject3" id="inlineRadio1" value="option1"> A
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject3" id="inlineRadio2" value="option2"> B
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject3" id="inlineRadio3" value="option3"> C
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="radiosubject3" id="inlineRadio3" value="option3" checked> S
                            </label>
                        </div>
                    </div>
                    <div id="warning"></div>
                    </div>

                                </br>
                                                        
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    </br>
                    <div>
                        <div class="row">
                            <div class="mycontainer">
                                <div class="form-login">
                                <h4>SELECT YOUR DESTINATION</h4>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="control-label" for="line1">Select Designation :</label>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control" name="desig" required="true">
                                        <option value="">Select</option>
                                        @if(isset($desigs))
                                            @foreach($desigs as $d)
                                                <option value="{{$d->designation_Id}}">{{$d->description}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                                </br>
                               
                                </div>
                            
                            <div>

                            </br>
                            <div class="form-group">

                                <div class="col-sm-4" style="float:right;">
                                    <input type="submit" class="btn btn-primary" value="Submit"/>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php /*
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Curriculum :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="cur">
                                    <option value="">Select</option>

                                    @if(isset($curs))
                                        @foreach($curs as $c)

                                            <option value="{{$c->curriculum_Id}}">{{$c->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Qualification :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="qual">
                                    <option value="">Select</option>
                                    @if(isset($quals))
                                        @foreach($quals as $q)
                                            <option value="{{$q->qualification_Id}}">{{$q->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label" for="line1">Select Designation :</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" name="desig">
                                    <option value="">Select</option>
                                    @if(isset($desigs))
                                        @foreach($desigs as $d)
                                            <option value="{{$d->designation_Id}}">{{$d->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-4">
                                <input type="submit" class="btn btn-primary" value="Submit"/>
                            </div>
                        </div>
                    */ ?>

                    </form>
                    </center>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var c = document.getElementById('stream');
        var d = document.getElementById('grade');

        d.addEventListener('change', function () {

            if (d.value == '13') {

                document.getElementById('stream').disabled = false;
            }
            else {
                document.getElementById('stream').disabled = true;
            }
        })


    </script>
@endsection
