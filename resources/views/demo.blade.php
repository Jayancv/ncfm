<DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Career Path</title>
        <h3> This is only demo, Not connected to DB</h3>
        <script src="js/jquery.min.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/training.css" rel="stylesheet"/>
        <script src="js/d3.js"></script>
    </head>
    <body>
    <div class='content'>
        <hr/>
        <div class="row">
            <div class="col-md-8"><span id='tree'></span></div>
            <div class="col-md-4">
                <div id='vid-container'>
                    <embed id='vid'/>
                </div>
            </div>
        </div>
        <script src='js/training-data.js'></script>
        <script src='js/training.js'></script>
    </body>

    </html>
