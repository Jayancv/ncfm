@extends('layouts.master')

@section('content')

    <div class="container-fluid row">

        <div class="col-md-12">
            <div class="panel panel-default ">
                <div class="panel-heading">Testing Form</div>
                <div class="panel-body col-sm-offset-1">
                    {{--<form class="form-horizontal" action="{{route('test')}}" method="post"--}}
                          {{--enctype="multipart/form-data" role="form">--}}
                        {{--{{csrf_field()}}--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4">Interested in</label>--}}
                            {{--<select type="text" name="stream" class=" col-md-4" id="stream">--}}
                                {{--<option value="Engineering">Engineering</option>--}}
                                {{--<option value="Management">Management</option>--}}
                                {{--<option value="ComputerScience">Computer Science</option>--}}
                                {{--<option value="Art">Art</option>--}}
                                {{--<option value="Politics">Politics</option>--}}
                                {{--<option value="Accounting">Accounting</option>--}}
                                {{--<option value="Medicine">Medicine</option>--}}
                                {{--<option value="Electronic">Electronic</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4">Your goal </label>--}}
                            {{--<select type="text" name="stream" class="col-md-4" id="stream">--}}
                                {{--<option value="Software Engineer">Software Engineer</option>--}}
                                {{--<option value="Bank Manager">Bank Manager</option>--}}
                                {{--<option value="Lawyer">Lawyer</option>--}}
                                {{--<option value="Architect">Architect</option>--}}
                                {{--<option value="Graphic  designer">Graphic  designer</option>--}}
                                {{--<option value="Accountant">Accountant</option>--}}
                                {{--<option value="Doctor">Doctor</option>--}}
                                {{--<option value="Civil Enginner">Civil Engineer</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4">Grade Completed</label>--}}
                            {{--<select name="grade" class="col-md-4" id="grade">--}}
                                {{--<option value="8">Grade 8</option>--}}
                                {{--<option value="9">Grade 9</option>--}}
                                {{--<option value="10">Grade 10</option>--}}
                                {{--<option value="11">O/L</option>--}}
                                {{--<option value="13">A/L</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4">A/L stream</label>--}}
                            {{--<select type="text" name="stream" class="col-md-4" id="stream">--}}
                                {{--<option value="Mathematics">Mathematics</option>--}}
                                {{--<option value="Biology">Biology</option>--}}
                                {{--<option value="Commerce">Commerce</option>--}}
                                {{--<option value="Art">Art</option>--}}
                                {{--<option value="Other">Other</option>--}}
                            {{--</select>--}}

                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4">Degree Completed</label>--}}
                            {{--<select name="grade" class="col-md-4" id="grade">--}}
                                {{--<option value="8">BSc.(Eng.)</option>--}}
                                {{--<option value="9">BSc.(Math.)</option>--}}
                                {{--<option value="10">LL.B.</option>--}}
                                {{--<option value="11">B.E</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group"></div>--}}
                        {{--<div class="form-group"></div>--}}


                        {{--<div class="col-md-4">--}}
                            {{--<button type="submit" class="btn btn-info  pull-right">Add</button>--}}

                        {{--</div>--}}
                    {{--</form>--}}


                    <form class="form-horizontal" role="form"  action="{{ route('process') }}" method="post">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="control-label" for="line1">Select Curriculum :</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="cur">
                                    <option value="">Select</option>

                                    @if(isset($curs))
                                        @foreach($curs as $c)

                                            <option value="{{$c->curriculum_Id}}">{{$c->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="control-label" for="line1">Select Qualification :</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="qual">
                                    <option value="">Select</option>
                                    @if(isset($quals))
                                        @foreach($quals as $q)
                                            <option value="{{$q->qualification_Id}}">{{$q->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="control-label" for="line1">Select Designation :</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="desig">
                                    <option value="">Select</option>
                                    @if(isset($desigs))
                                        @foreach($desigs as $d)
                                            <option value="{{$d->designation_Id}}">{{$d->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-4">
                                <input type="submit" class="btn btn-primary" value="Submit" />
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var c = document.getElementById('stream');
        var d = document.getElementById('grade');

        d.addEventListener('change', function () {

            if (d.value == '13') {

                document.getElementById('stream').disabled = false;
            }
            else {
                document.getElementById('stream').disabled = true;
            }
        })


    </script>
@endsection
