<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paths extends Model
{
    //
    protected $table = "paths";
    protected $primaryKey = "path_Id";

    public function startNode(){
        return $this->belongsTo('App\Node', 'start_Node', 'node_Id');
    }

    public function endNode(){
        return $this->belongsTo('App\Node', 'end_Node', 'node_Id');
    }

    public function getPathId(){
        return $this->path_id;
    }
    public function getStartNode()
    {
        return $this->start_Node;
    }

    public function getEndNode()
    {
        return $this->end_Node;
    }

    public function getPathDescription()
    {
        return $this->description;
    }
}
