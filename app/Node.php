<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    //
    protected $table = "node";
    protected $primaryKey = "node_Id";
    public $incrementing = false;

    public function curriculum(){
        return $this->belongsTo('App\Curriculum', 'curriculum_Id', 'curriculum_Id');
    }
    public function qualification(){
        return $this->belongsTo('App\Qualification', 'qualification_Id', 'qualification_Id');
    }
    public function startNode(){
        return $this->hasMany('App\Paths', 'node_Id', 'start_Node');
    }
    public function endNode(){
        return $this->hasMany('App\Paths', 'node_Id', 'end_Node');
    }
    public function designation(){
        return $this->hasMany('App\Node_Designations', 'node_Id', 'node_Id');
    }


    public function getNodeId(){
        return $this->node_Id;
    }
    public function getCurriculumId(){
        return $this->curriculum_Id;
    }
    public function getQualificationId(){
        return $this->qualification_Id;
    }
    public function getNodeDescription(){
        return $this->description;
    }
}
