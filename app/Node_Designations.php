<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node_Designations extends Model
{
    //
    protected $table = "node_designations";
//    protected $primaryKey = "designation_Id,node_Id";

    public function designation(){
        return $this->belongsTo('App\Designation', 'designation_Id', 'designation_Id');
    }
    public function node(){
        return $this->belongsTo('App\Node', 'node_Id', 'node_Id');
    }
    public static function getNodeByDesignation($designationId){
        return Node_Designations::where('designation_Id', '=',$designationId)->get();
    }

    public function getDesignationId(){
        return $this->designation_Id;
    }
    public function  getNodeId(){
        return $this->node_id;
    }
}
