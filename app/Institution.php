<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    //
    protected $table = "institutions";
    protected $primaryKey = "institution_Id";

    public function course(){
        return $this->hasMany('App\Course_Institution', 'institution_Id', 'institution_Id');
    }
    
    public function getInstitutionId(){
        return $this->institution_Id;
    }
    public function getInstitutionDescription(){
        return $this->description;
    }
}
