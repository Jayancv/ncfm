<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Path_Course extends Model
{
    //
    protected $table = "path_courses";
   // protected $primaryKey = "path_Id,course_Id";

    public function path(){
        return $this->belongsTo('App\Paths', 'path_Id', 'path_Id');
    }
    public function course(){
        return $this->belongsTo('App\Courses', 'course_Id', 'course_Id');
    }
    
    
    public function getPathId(){
        return $this->path_Id;
    }
    public function getCourseId(){
        return $this->course_Id;
    }
}
