<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum_Group extends Model
{
    //
    protected $table = "curriculum_group";
    protected $primaryKey = "group_Id";

    public function course(){
        return $this->hasMany('App\Curriculum', 'group_Id', 'group_Id');
    }
    
    public static function getAllGroups(){
        return Curriculum_Group::all();
    }

    public function getGroupId(){
        return $this->group_Id;
    }
    public function getGroupDescription(){
        return $this->description;
    }
}
