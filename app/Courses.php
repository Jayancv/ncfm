<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    //
    protected $table = "courses";
    protected $primaryKey = "course_Id";

    public function institution(){
        return $this->hasMany('App\Course_Institution', 'course_Id', 'course_Id');
    }
    public function path(){
        return $this->hasMany('App\Path_Course', 'course_Id', 'course_Id');
    }

    public function getCourseId(){
        return $this->course_Id;
    }
    public function getCourseDescription(){
        return $this->description;
    }
}
