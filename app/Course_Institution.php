<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Institution extends Model
{
    //
    protected $table = "course_institutions";
    protected $primaryKey = "Id";

    public function institution()
    {
        return $this->belongsTo('App\Institution', 'institution_Id', 'institution_Id');
    }

    public function course()
    {
        return $this->belongsTo('App\Courses', 'course_Id', 'course_Id');
    }

    public static function getACourese_institution($id)
    {
        return Course_Institution::find($id);

    }

    public function getCourseInstitutionId()
    {
        return $this->Id;
    }

    public function getInstitutionId()
    {
        return $this->institution_Id;
    }

    public function getCourseId()
    {
        return $this->course_Id;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getDuration()
    {
        return $this->duration;
    }
}
