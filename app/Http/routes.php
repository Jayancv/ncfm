<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/learningPath', 'HomeController@getInputs');
Route::post('/learningPath', ['as' => 'learningPath', 'uses' => 'SelectController@getNodes']);


Route::get('/test', function () {
    return view('Testing.test');
});
Route::post('/test', ['as' => 'test', 'uses' => 'InstitutionController@create']);

Route::post('/demo', 'ProcessController@findPath')->name('process');
Route::get('/demoView', 'ProcessController@viewSelection');


Route::post('/test', ['as' => 'test', 'uses' => 'InstitutionController@create']);
Route::get('/details/{id}', 'InstitutionController@view')->name('institute_details');
