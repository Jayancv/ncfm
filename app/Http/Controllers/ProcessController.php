<?php

namespace App\Http\Controllers;

use App\Node_Designations;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Node;
use Log;
use Exception;


class ProcessController extends Controller
{
    public function viewSelection()
    {
        $curs = \App\Curriculum::all();
        $quals = \App\Qualification::all();
        $desigs = \App\Designation::all();
        $al_subjects = \App\Al_Subject::all();

        return View::make('demoView')->with('curs', $curs)
            ->with('quals', $quals)
            ->with('desigs', $desigs)
            ->with('al_subjects', $al_subjects);
    }

    public function findPath(Request $request)
    {
        $cur = $request->cur;
        $qual = $request->qual;
        $desig = $request->desig;

        $st_node = Node::where('curriculum_Id', '=', $cur)
            ->where('qualification_Id', '=', $qual)->first();

        if ($st_node == null) {
            return redirect('/demoView');
        }

        //find the end node
        $en_node = Node_Designations::where('designation_Id', '=', $desig)->get();


        if ($en_node == null) {
            return redirect('/demoView');
        }

        //get all the nodes down for now, later graph search can be
        //ported to mysql
        // $paths = DB::select("SELECT * FROM paths");
        $paths = DB::select("select p.path_Id,p.start_Node,p.end_Node, MIN(ci.duration) as duration, MIN(ci.cost) as cost from paths p
inner join path_courses pc on p.path_Id = pc.path_Id
inner join course_institutions ci on pc.course_Id = ci.course_Id
GROUP by p.path_Id");

        //adjascency matrix, directed and paths as values
        $matrix = array();

        //prepare the adj matrix for dfs
        for ($i = 0; $i < count($paths); $i++) {
            $n1 = $paths[$i]->start_Node;
            $n2 = $paths[$i]->end_Node;
            $p = $paths[$i]->path_Id;
            $cost = $paths[$i]->cost;
            $duration = $paths[$i]->duration;

            if (!array_key_exists($n1, $matrix)) {
                $matrix[$n1] = array();
            }
            if (!array_key_exists($n2, $matrix[$n1])) {
                $matrix[$n1][$n2] = array(
                    "p" => $p,
                    "visited" => false,
                    "cost" => $cost,
                    "duration" => $duration
                    );
            }
        }
        $this->shortest_path = array(
              "cost" => array(
                    "sp_path" => null,
                    "sp_cost" => 100000000
              ),
              "duration" => array(
                    "sp_path" => null,
                    "sp_cost" => 100000000
              )
        );
        $paths_by_cost = $this->getReturnDics($matrix,$st_node,$en_node,"cost");
        $paths_by_duration = $this->getReturnDics($matrix,$st_node,$en_node,"duration");

        //dd(array($paths_by_cost,$paths_by_duration));

        return View::make('showPaths')->with("paths", array($paths_by_cost,$paths_by_duration));

        /*
        //DFS and find all the different paths
        $this->pathSet = array();
        foreach ($en_node as $n) {
            $en_node = $n->node;
            $this->dfs($matrix, array($st_node->node_Id), $en_node->node_Id, $st_node->node_Id, 0, "cost");
            //$this->dfs($matrix, array($st_node->node_Id), $en_node->node_Id, $st_node->node_Id, 0, "duration");
        }
        //dd($this->pathSet);
        //create dictionary with the related nodes
        $node_dic = array();
        //create a dictionary of all the nodes in the dic
        $node_set = array();

        foreach ($this->pathSet as $pathary) {
          $path = $pathary["path"];
          for($i=1;$i<count($path);$i++){
            //if the parent node of current node is not in the matrix add it
            if(!array_key_exists($path[$i-1],$node_dic)){
              $node_dic[$path[$i -1]] = array();
            }
            if(!in_array($path[$i],$node_dic[$path[$i -1]])){
              array_push($node_dic[$path[$i -1]],$path[$i]);
            }
            $node_set[$path[$i]] = null;
            $node_set[$path[$i-1]] = null;
          }
        }

        //$node_set = array_unique($node_set);

        foreach ($node_set as $node_Id=>$i) {
          $node = Node::find($node_Id);
          $node_set[$node_Id] = $node->description;
        }

        //dd($node_dic);

        $reduced_matrix = array();
        foreach ($node_dic as $n1 => $n1_nodes) {
            $reduced_matrix[$n1] = array();
            foreach ($n1_nodes as $key => $n2) {
                $reduced_matrix[$n1][$n2] = array();
                $p = $matrix[$n1][$n2]['p'];


                //$path_institute = Db::select('SELECT c.description as course, i.description as inst, ci.id as cid FROM institutions i, courses c,course_institutions ci,paths p, path_courses pc WHERE ci.course_Id = c.course_Id and ci.institution_Id = i.institution_Id and pc.course_Id = c.course_Id and pc.path_Id = p.path_Id and p.path_Id = "'.$p.'"');
                $path_institute = Db::select('SELECT c.description as course, i.description as inst, ci.Id as inst_crs_id FROM institutions i, courses c,course_institutions ci,paths p, path_courses pc WHERE ci.course_Id = c.course_Id and ci.institution_Id = i.institution_Id and pc.course_Id = c.course_Id and pc.path_Id = p.path_Id and p.path_Id = "'.$p.'"');




                $insts = array();
                $course = "";
                foreach ($path_institute as $i) {
                    $course =  $i->course;

                    array_push($insts, array('name' => $i->inst,'id' => $i->inst_crs_id));
                }

                $reduced_matrix[$n1][$n2] = array('path_id' => $p,
                                                  'course' => $course,
                                                  'institutions' => $insts);

            }
        }

        //dd($reduced_matrix);

        return View::make('showPaths')->with('node_dic',$node_dic)
                                      ->with('node_set',$node_set)
                                      ->with('start_node',$st_node->node_Id)
                                      ->with('end_node',$en_node->node_Id)
                                      ->with('path_info', $reduced_matrix);
        */



    }

    function getReturnDics($matrix,$st_node,$en_node,$cost_by){
        //DFS and find all the different paths
        $this->pathSet = array();
        foreach ($en_node as $n) {
            $en_node = $n->node;
            $this->dfs($matrix, array($st_node->node_Id), $en_node->node_Id, $st_node->node_Id, 0, $cost_by);
            //$this->dfs($matrix, array($st_node->node_Id), $en_node->node_Id, $st_node->node_Id, 0, "duration");
        }
        //dd($this->pathSet);
        //create dictionary with the related nodes
        $node_dic = array();
        //create a dictionary of all the nodes in the dic
        $node_set = array();

        foreach ($this->pathSet as $pathary) {
          $path = $pathary["path"];
          for($i=1;$i<count($path);$i++){
            //if the parent node of current node is not in the matrix add it
            if(!array_key_exists($path[$i-1],$node_dic)){
              $node_dic[$path[$i -1]] = array();
            }
            if(!in_array($path[$i],$node_dic[$path[$i -1]])){
              array_push($node_dic[$path[$i -1]],$path[$i]);
            }
            $node_set[$path[$i]] = null;
            $node_set[$path[$i-1]] = null;
          }
        }

        //$node_set = array_unique($node_set);

        foreach ($node_set as $node_Id=>$i) {
          $node = Node::find($node_Id);
          $node_set[$node_Id] = $node->description;
        }

        //dd($node_dic);

        $reduced_matrix = array();
        foreach ($node_dic as $n1 => $n1_nodes) {
            $reduced_matrix[$n1] = array();
            foreach ($n1_nodes as $key => $n2) {
                $reduced_matrix[$n1][$n2] = array();
                $p = $matrix[$n1][$n2]['p'];

                $path_institute = Db::select('SELECT c.description as course, i.description as inst, ci.Id as inst_crs_id FROM institutions i, courses c,course_institutions ci,paths p, path_courses pc WHERE ci.course_Id = c.course_Id and ci.institution_Id = i.institution_Id and pc.course_Id = c.course_Id and pc.path_Id = p.path_Id and p.path_Id = "'.$p.'"');

                $insts = array();
                $course = "";
                foreach ($path_institute as $i) {
                    $course =  $i->course;

                    array_push($insts, array('name' => $i->inst,'id' => $i->inst_crs_id));
                }

                $reduced_matrix[$n1][$n2] = array('path_id' => $p,
                                                  'course' => $course,
                                                  'institutions' => $insts);

            }
        }

        return array(
                    "node_dic" => $node_dic,
                    "node_set" => $node_set,
                    "start_node" => $st_node->node_Id,
                    "end_node" => $en_node->node_Id,
                    "path_info" => $reduced_matrix,
                    "shortest_path" => $this->shortest_path[$cost_by]
        );
    }

    public function adjescentNodes($matrix, $node)
    {
        $keys = array();
        if (!array_key_exists($node, $matrix)) {
            return $keys;
        }
        foreach ($matrix[$node] as $key => $value) {
            if ($value['visited'] === false) {
                array_push($keys, $key);
            }
        }
        return $keys;
    }

    public function dfs($matrix, $curPath, $end, $node, $path_cost, $cost_by = "cost")
    {
        //array_push($curPath, $node);

        if ($node === $end) {
            //array_push($this->pathSet, $curPath);

            if($path_cost < $this->shortest_path[$cost_by]['sp_cost']){
              $this->shortest_path[$cost_by]['sp_path'] = $curPath;
              $this->shortest_path[$cost_by]['sp_cost'] = $path_cost;
            }
            array_push($this->pathSet, array(
                                            "path" => $curPath,
                                            "path_cost" => $path_cost));
            return;
        }

        $nodes = $this->adjescentNodes($matrix, $node);
        foreach ($nodes as $nextNode) {
            $matrix[$node][$nextNode]['visited'] = true;
            array_push($curPath, $nextNode);

            $this->dfs($matrix, $curPath, $end, $nextNode, $path_cost+$matrix[$node][$nextNode][$cost_by],$cost_by);

            $matrix[$node][$nextNode]['visited'] = false;
            array_pop($curPath);
        }

    }
}
