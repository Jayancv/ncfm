<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use Exception;

use App\Node_Designations;

use Illuminate\Support\Facades\DB;


class SelectController extends Controller
{
    //
    public function getNodes(Request $request)
    {
        $this->pathSets = array();

        Log::info($request->all());
        $endNodes = Node_Designations::getNodeByDesignation($request->designation);
        foreach ($endNodes as $endNode) {
            $endNodeId = $endNode->node_Id;
            $this->findPath($endNodeId);
        }
        dd($this->pathSets);
    }

    public function findPath($end)
    {
        //get all the nodes down for now, later graph search can be
        //ported to mysql
        $paths = DB::select("SELECT * FROM paths");
        //dd($paths);
        //adjascency matrix, directed and paths as values
        $matrix = array();

        for ($i = 0; $i < count($paths); $i++) {
            $n1 = $paths[$i]->start_Node;
            $n2 = $paths[$i]->end_Node;
            $p = $paths[$i]->path_Id;

            if (!array_key_exists($n1, $matrix)) {
                $matrix[$n1] = array();
            }
            if (!array_key_exists($n2, $matrix[$n1])) {
                $matrix[$n1][$n2] = array("p" => $p,
                    "visited" => false);
            }
        }

        $this->pathSet = array();
        $this->dfs($matrix, array(), $end, 'N7');

//            dd($this->pathSet);
        array_push($this->pathSets, $this->pathSet);
        return;

    }

    public function adjescentNodes($matrix, $node)
    {
        $keys = array();
        if (!array_key_exists($node, $matrix)) {
            return $keys;
        }
        foreach ($matrix[$node] as $key => $value) {
            if ($value['visited'] === false) {
                array_push($keys, $key);
            }
        }
        return $keys;
    }

    public function dfs($matrix, $curPath, $end, $node)
    {
        //array_push($curPath, $node);

        if ($node === $end) {
            array_push($this->pathSet, $curPath);
            return;
        }

        $nodes = $this->adjescentNodes($matrix, $node);
        foreach ($nodes as $nextNode) {
            $matrix[$node][$nextNode]['visited'] = true;
            array_push($curPath, $matrix[$node][$nextNode]['p']);

            $this->dfs($matrix, $curPath, $end, $nextNode);

            $matrix[$node][$nextNode]['visited'] = false;
            array_pop($curPath);
        }
    }

}
