<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Designation;
use App\Curriculum_Group;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getInputs(){

        $designations = Designation::getAllDesignations();
        $groups = Curriculum_Group::getAllGroups();
        Log::info($designations);
        return view('/learningPath', ['designations' => $designations, 'groups' => $groups]);
    }
    
}
