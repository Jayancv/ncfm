<?php

namespace App\Http\Controllers;

use App\Course_Institution;
use Illuminate\Http\Request;

use App\Http\Requests;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Log;
use Exception;

use App\Institution;


class InstitutionController extends Controller
{
    //
    public function create(Request $request)
    {
        $institution = new Institution();

        $institution->institution_id = $request->id;
        $institution->description=$request->description;
        
        DB::beginTransaction();
        try {
            $institution->save();

        } catch (Exception $e) {
            LOg::info($e);
            DB::rollback();
            return back()->with('errors', array("Something went wrong!"));
        }
        Log::info('done');
        DB::commit();

       
        return back()->with('success', $request->title . " updated successfully!");
    }

    public function view($id){
       $details= Course_Institution::getACourese_institution($id);
//        Log::info($details);
        return view('details',['details' => $details]);

    }
}
