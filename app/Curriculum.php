<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    //
    protected $table = "curriculum";
    protected $primaryKey = "curriculum_Id";
    public $incrementing = false;

    public function group(){
        return $this->belongsTo('App\Curriculum_Group', 'group_Id', 'group_Id');
    }

    public function node(){
        return $this->hasOne('App\Node', 'curriculum_Id', 'curriculum_Id');
    }

    public function getCurriculumId(){
        return $this->curriculum_Id;
    }
    public function groupId(){
        return $this->group_Id;
    }
    public function getCurriculumDescription(){
        return $this->description;
    }
}

