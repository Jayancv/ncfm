<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Al_Subject extends Model
{
    //
     protected $table = 'al_subject';
     protected $primaryKey = "al_subject_id";
     public $timestamps = false;
}
