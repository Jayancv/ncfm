<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    //
    protected $table = "designation";
    public $incrementing = false;
//    protected $primaryKey = "designation_Id";

    public function course(){
        return $this->hasMany('App\Node_Designations', 'designation_Id', 'designation_Id');
    }

    public static function getAllDesignations(){
        return Designation::all();
    }

    public function getDesignationId(){
        return $this->designation_Id;
    }
    public function getDesignationDescription(){
        return $this->description;
    }
}
